import React from 'react';
import styles from './Footer.module.css';
import { ReactComponent as Dragon } from '../Assets/dragon.svg';

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <Dragon />
      <p>Todos os direitos reservados aos Dragões.</p>
    </footer>
  );
};

export default Footer;

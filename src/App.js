import React from 'react';
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Header from './Components/Header';
import Footer from './Components/Footer';
import Home from './Components/Home';
import Login from './Components/Login/Login';
import { UserStorage } from './UserContext';
import User from './Components/User/User';
import Dragon from './Components/Dragon/Dragon';
import ProtectedRoute from './Components/Helper/ProtectedRoute';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <UserStorage>
          <Header />
          <main className="AppBody">
            <Routes>
              <ProtectedRoute path="/" element={<Home />} />
              <Route path="login/*" element={<Login />} />
              <Route path="dragon/:id" element={<Dragon />} />
              <ProtectedRoute path="conta/*" element={<User />} />
            </Routes>
          </main>
          <Footer />
        </UserStorage>
      </BrowserRouter>
    </div>
  );
}

export default App;
